package com.example.reproductor;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button play_pause, btn_repeat;
    MediaPlayer mp;
    ImageView iv;
    int repeat = 2, position = 0;

    MediaPlayer[] arrayed = new MediaPlayer[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        play_pause =  (Button)findViewById(R.id.btn_play);
        btn_repeat =  (Button)findViewById(R.id.btn_repeat);
        iv = (ImageView)findViewById(R.id.imageView);

        arrayed[0] = MediaPlayer.create(this, R.raw.alegria);
        arrayed[1] = MediaPlayer.create(this, R.raw.trapstorno);
        arrayed[2] = MediaPlayer.create(this, R.raw.mlqhd);
        arrayed[3] = MediaPlayer.create(this, R.raw.quiencontrami);
        arrayed[4] = MediaPlayer.create(this, R.raw.stamina);
    }

    // Method for play pause

    public void PlayPause(View view){
        if (arrayed[position].isPlaying()){
            arrayed[position].pause();
            play_pause.setBackgroundResource(R.drawable.reproducir);
            Toast.makeText(this, "Pause", Toast.LENGTH_SHORT);
        } else {
            arrayed[position].start();
            play_pause.setBackgroundResource(R.drawable.pausa);
            Toast.makeText(this, "Play", Toast.LENGTH_SHORT);
        }
    }

    // Method for Stop
    public  void Stop(View view){
        if (arrayed[position] != null){
            arrayed[position].stop();

            arrayed[0] = MediaPlayer.create(this, R.raw.alegria);
            arrayed[1] = MediaPlayer.create(this, R.raw.trapstorno);
            arrayed[2] = MediaPlayer.create(this, R.raw.mlqhd);
            arrayed[3] = MediaPlayer.create(this, R.raw.quiencontrami);
            arrayed[4] = MediaPlayer.create(this, R.raw.stamina);

            position = 0;
            play_pause.setBackgroundResource(R.drawable.reproducir);
            iv.setImageResource(R.drawable.portadaalegria);
            Toast.makeText(this,"Stop", Toast.LENGTH_SHORT).show();
        }
    }

    // Method for repeat
    public void Repeat(View view){
        if (repeat == 1){
            btn_repeat.setBackgroundResource(R.drawable.no_repetir);
            Toast.makeText(this,"No Repetir", Toast.LENGTH_SHORT).show();
            arrayed[position].setLooping(false);
            repeat = 2;
        }else{
            btn_repeat.setBackgroundResource(R.drawable.repetir);
            Toast.makeText(this,"Repetir", Toast.LENGTH_SHORT).show();
            arrayed[position].setLooping(true );
            repeat = 1;
        }

    }

    // Method for next song
    public void NextSong(View view){
        if (position < arrayed.length -1){

            if (arrayed[position].isPlaying()){
                arrayed[position].stop();
                position++;
                arrayed[position].start();

                if (position == 0){
                    iv.setImageResource(R.drawable.portadaalegria);
                }
                if (position == 1){
                    iv.setImageResource(R.drawable.portadatrapstorno);
                }
                if (position == 2){
                    iv.setImageResource(R.drawable.portadamlqhd);
                }
                if (position == 3){
                    iv.setImageResource(R.drawable.portadaquiencontrami);
                }
                if (position == 4){
                    iv.setImageResource(R.drawable.portadastamina);
                }

            }else{
                position++;
                if (position == 0){
                    iv.setImageResource(R.drawable.portadaalegria);
                }
                if (position == 1){
                    iv.setImageResource(R.drawable.portadatrapstorno);
                }
                if (position == 2){
                    iv.setImageResource(R.drawable.portadamlqhd);
                }
                if (position == 3){
                    iv.setImageResource(R.drawable.portadaquiencontrami);
                }
                if (position == 4){
                    iv.setImageResource(R.drawable.portadastamina);
                }
            }

        }else{
            Toast.makeText(this, "No hay más canciones", Toast.LENGTH_SHORT).show();
        }
    }

    //Method fot before
    public void BeforeSong(View view){
        if (position >=1 ){

            if(arrayed[position].isPlaying()){
                arrayed[position].stop();
                arrayed[0] = MediaPlayer.create(this, R.raw.alegria);
                arrayed[1] = MediaPlayer.create(this, R.raw.trapstorno);
                arrayed[2] = MediaPlayer.create(this, R.raw.mlqhd);
                arrayed[3] = MediaPlayer.create(this, R.raw.quiencontrami);
                arrayed[4] = MediaPlayer.create(this, R.raw.stamina);
                position--;

                if (position == 0){
                    iv.setImageResource(R.drawable.portadaalegria);
                }
                if (position == 1){
                    iv.setImageResource(R.drawable.portadatrapstorno);
                }
                if (position == 2){
                    iv.setImageResource(R.drawable.portadamlqhd);
                }
                if (position == 3){
                    iv.setImageResource(R.drawable.portadaquiencontrami);
                }
                if (position == 4){
                    iv.setImageResource(R.drawable.portadastamina);
                }

                arrayed[position].start();
            }else{
                position--;
                if (position == 0){
                    iv.setImageResource(R.drawable.portadaalegria);
                }
                if (position == 1){
                    iv.setImageResource(R.drawable.portadatrapstorno);
                }
                if (position == 2){
                    iv.setImageResource(R.drawable.portadamlqhd);
                }
                if (position == 3){
                    iv.setImageResource(R.drawable.portadaquiencontrami);
                }
                if (position == 4){
                    iv.setImageResource(R.drawable.portadastamina);
                }
            }

        }else{
            Toast.makeText(this, "No hay más canciones", Toast.LENGTH_SHORT).show();
        }
    }


}